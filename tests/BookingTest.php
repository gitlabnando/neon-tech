<?php

namespace Tests;

use PHPUnit\Framework\TestCase;
use Src\controllers\Booking;
use Src\models\BookingModel;
use Src\models\ClientModel;

class BookingTest extends TestCase
{
    private $booking;

    /**
     * Setting default data
     * @throws \Exception
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->booking = new Booking();
    }

    /** @test */
    public function getBookings()
    {
        $results = $this->booking->getBookings();

        $this->assertIsArray($results);
        $this->assertIsNotObject($results);

        $this->assertEquals($results[0]['id'], 1);
        $this->assertEquals($results[0]['clientid'], 1);
        $this->assertEquals($results[0]['price'], 200);
        $this->assertEquals($results[0]['checkindate'], '2021-08-04 15:00:00');
        $this->assertEquals($results[0]['checkoutdate'], '2021-08-11 15:00:00');
    }

    /** @test */
    public function bookNewClient()
    {
        $clientModel = new ClientModel();
        $bookingModel = new BookingModel();

        $_POST['checkInDate'] = '2021-08-04 15:00:00';
        $_POST['checkOutDate'] = '2021-08-11 15:00:00';
        $_POST['price'] = 200;
        $_POST['username'] = 'fernandouser';
        $_POST['name'] = 'Fernando';
        $_POST['email'] = 'example@example.com';
        $_POST['phone'] = '555198312145';

        $result = $this->booking->bookNewClient($clientModel, $bookingModel);
        $this->assertSame('booked', $result);
    }

    /** @test 
    *  @dataProvider getData
    */
    public function dontBookNewClientWithInvalidData(array $data)
    {
        $clientModel = new ClientModel();
        $bookingModel = new BookingModel();

        $_POST['checkInDate'] = $data['checkInDate'];
        $_POST['checkOutDate'] = $data['checkOutDate'];
        $_POST['price'] = $data['price'];
        $_POST['username'] = 'fernandouser';
        $_POST['name'] = 'Fernando';
        $_POST['email'] = 'example@example.com';
        $_POST['phone'] = '555198312145';

        $result = $this->booking->bookNewClient($clientModel, $bookingModel);
        $this->assertSame('Data is invalid', $result);
    }

    /** return @dataProvider */
    private function getData()
    {
        return [
            'price is null' => [
                [
                    'checkInDate' => '1212312',
                    'price' => null,
                    'checkOutDate' => '21213123',
                ]
            ],
            'checkInDate is null' => [
                [
                    'checkInDate' => null,
                    'price' => 200,
                    'checkOutDate' => '21213123',
                ]
            ],
            'checkOutDate is null' => [
                [
                    'checkInDate' => '121332',
                    'price' => 200,
                    'checkOutDate' => null,
                ]
            ],
        ];
    }
}
