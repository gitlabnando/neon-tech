<?php

namespace Src\controllers;

use Src\models\BookingModel;
use Src\models\ClientModel;

class Booking {

	private function getBookingModel(): BookingModel {
		return new BookingModel();
	}

	public function getBookings() {
		return $this->getBookingModel()->getBookings();
	}

	public function bookNewClient(ClientModel $clientModel, BookingModel $bookingModel)
	{
		$checkInDate = $_POST['checkInDate'];
		$checkOutDate = $_POST['checkOutDate'];
		$price = $_POST['price'];
		$username = $_POST['username'];
        $name = $_POST['name'];
        $email = $_POST['email'];
        $phone = $_POST['phone'];

		if (!$price || !$checkInDate || !$checkOutDate) {
			return 'Data is invalid';
		}

		$client = $clientModel->createClient(compact('username', 'name', 'email', 'phone'));
		$booked = $bookingModel->book($client, $checkInDate, $checkOutDate, $price);

		return $booked === false ? 'not booked' : 'booked';
	}
}