<?php

namespace Src\models;

use Src\helpers\Helpers;

class BookingModel {

	private $bookingData;
	private Helpers $helper;

	function __construct() {
		$string = file_get_contents(dirname(__DIR__) . '/../scripts/bookings.json');
		$this->bookingData = json_decode($string, true);
		$this->helper = new Helpers();
	}

	public function getBookings() {
		return $this->bookingData;
	}

	public function book(array $client, string $checkInDate, string $checkOutDate, float $price): int|false
	{
		$id = end($this->bookingData)['id'];

		$this->bookingData[] = [
			'id' => $id++,
			'clientid' => $client['id'],
			'price' => $price,
			'checkindate' => $checkInDate,
			'checkoutdate' => $checkOutDate,
			'checkoutdate' => $checkOutDate,
		];

		return $this->helper->putJson($this->bookingData, 'bookings');
	}
}